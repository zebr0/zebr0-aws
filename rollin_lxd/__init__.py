import helixd
import kivalu
import whassup
import yaml
from blinker import signal

import rollin

STACK = """
{% set name = "-".join(levels) or "rollin" %}
---
storage-pools:
- name: {{ name }}
  driver: {{ "rollin-lxd-storage-driver" | get("zfs") }}

networks:
- name: {{ name }}
  type: bridge
  config:
    ipv4.address: {{ "rollin-lxd-network-cidr" | get("192.168.42.1/24") }}
    ipv4.nat: true
    ipv6.address: none

profiles:
- name: {{ name }}
  devices:
    root:
      type: disk
      path: /
      pool: {{ name }}
    eth0:
      type: nic
      nictype: bridged
      parent: {{ name }}

volumes:
- name: {{ name }}
  parent: {{ name }}
""".lstrip()

INSTANCE = """
{% set name = "-".join(levels) or "rollin" %}
---
name: {{ name }}
source:
  type: image
  mode: pull
  server: {{ "rollin-lxd-image-server" | get("https://images.linuxcontainers.org") }}
  protocol: simplestreams
  alias: {{ "rollin-lxd-image-alias" | get("ubuntu/jammy/cloud") }}
profiles:
- {{ name }}
devices:
  backup:
    type: disk
    pool: {{ name }}
    source: {{ name }}
    path: {{ "rollin-lxd-restic-repository" | get("/var/tmp/backup") }}
config:
  user.user-data: |
    #cloud-config
    write_files:
    - path: /etc/kivalu.conf
      content: |
        {{ configuration }}
    runcmd:
    - add-apt-repository -y ppa:zebr0/omega
    - apt-get -y install pummel
    - pummel run
""".lstrip()


class Client(rollin.Client):
    def __init__(self, kivalu_client: kivalu.Client):
        super().__init__(kivalu_client)

        self.helixd_client = helixd.Client(kivalu_client.get("rollin-lxd-api-url", helixd.URL_DEFAULT))
        self.stack = yaml.load(kivalu_client.get("rollin-lxd-stack", STACK), Loader=yaml.BaseLoader)
        self.instance = yaml.load(kivalu_client.get("rollin-lxd-instance", INSTANCE), Loader=yaml.BaseLoader)

    def create(self):
        self.helixd_client.create_stack(self.stack)

        if not self.helixd_client.exists(self.instance):
            self.helixd_client.create(self.instance)
        if not self.helixd_client.is_running(self.instance):
            self.helixd_client.start(self.instance)
            whassup.warning_tolerant_check(f"https://{self.get_address()}:2501", None, ["nominal"], ignore_tls=True)

            signal("started").send(self)

    def get_address(self):
        return self.helixd_client.get_ip_address(self.instance)

    def rollin_if_ready(self):
        if whassup.warning_tolerant_check(f"https://{self.get_address()}:2501", None, ignore_tls=True).mode == "rollin":
            self.helixd_client.stop(self.instance)
            self.helixd_client.delete(self.instance)

            self.create()

    def delete(self):
        if whassup.warning_tolerant_check(f"https://{self.get_address()}:2501", None, ignore_tls=True).mode == "rollin":
            self.helixd_client.stop(self.instance)
            self.helixd_client.delete(self.instance)
            self.helixd_client.delete_stack(self.stack)

            signal("deleted").send(self)
