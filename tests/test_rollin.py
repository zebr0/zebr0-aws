import threading
import time

import rollin
from tests import rollin_mock


def test_configuration_directory_not_exists(tmp_path, capsys):
    configuration_directory = tmp_path.joinpath("not-exists")

    rollin.rollin(configuration_directory, threading.Event())

    assert capsys.readouterr().out == f"exiting: {configuration_directory} must exist and be a directory\n"


def test_configuration_directory_not_a_directory(tmp_path, capsys):
    configuration_directory = tmp_path.joinpath("file")
    configuration_directory.touch()

    rollin.rollin(configuration_directory, threading.Event())

    assert capsys.readouterr().out == f"exiting: {configuration_directory} must exist and be a directory\n"


WRONG_PATTERN_OUTPUT = """
ModuleNotFoundError("No module named 'wrong'")
exiting: loop-ending signal
""".lstrip()


def test_wrong_pattern(tmp_path, monkeypatch, capsys):
    monkeypatch.setattr(rollin, "read_configuration", lambda _: None)
    monkeypatch.setattr(rollin, "validate", lambda _: [{"pattern": "wrong"}])

    event = threading.Event()

    def sleep_then_stop():
        time.sleep(0.1)
        event.set()

    threading.Thread(target=sleep_then_stop).start()

    rollin.rollin(tmp_path, event, 1)

    assert capsys.readouterr().out == WRONG_PATTERN_OUTPUT


OK_OUTPUT = """
alpha already deleted
beta already deleted
alpha creating
beta already deleted
beta already deleted
beta creating
alpha created
alpha not rollin yet
alpha already exists
alpha not rollin yet
alpha already exists
alpha not rollin yet
beta created
beta not rollin yet
alpha already exists
alpha rollin
beta already exists
beta not rollin yet
beta already exists
beta not rollin yet
beta already exists
beta rollin
alpha rolled
alpha already exists
alpha not rollin yet
alpha already exists
alpha not rollin yet
beta rolled
alpha already exists
alpha not rollin yet
beta already exists
beta not rollin yet
alpha deleting
beta already exists
beta not rollin yet
beta already exists
beta not rollin yet
beta deleting
alpha deleted
alpha already deleted
alpha already deleted
beta deleted
alpha already deleted
beta already deleted
exiting: loop-ending signal
""".lstrip()


def test_ok(tmp_path, monkeypatch, capsys):
    with rollin_mock.TestServer():
        config_alpha = {"levels": ["alpha"], "pattern": "rollin_mock", "disabled": "true"}
        config_beta = {"levels": ["beta"], "pattern": "rollin_mock", "disabled": "true"}
        monkeypatch.setattr(rollin, "read_configuration", lambda _: None)
        monkeypatch.setattr(rollin, "validate", lambda _: [config_alpha, config_beta])

        event = threading.Event()

        def sleep_then_stop():
            time.sleep(0.1)
            config_alpha.update({"disabled": "false"})
            time.sleep(0.6)
            config_beta.update({"disabled": "false"})
            time.sleep(3)
            config_alpha.update({"disabled": "true"})
            time.sleep(0.6)
            config_beta.update({"disabled": "true"})
            time.sleep(1.2)
            event.set()

        threading.Thread(target=sleep_then_stop).start()

        rollin.rollin(tmp_path, event, 0.3)

        assert capsys.readouterr().out == OK_OUTPUT
