import pathlib

import rollin

READ_CONFIGURATION_FILE_1 = """
---
- lorem
- ipsum
- dolor
- sit
- amet
""".lstrip()

READ_CONFIGURATION_FILE_2 = """
---
- consectetur
- adipiscing
- elit
""".lstrip()


def test_read_configuration_ok(tmp_path, capsys):
    tmp_path.joinpath("file1.yml").write_text(READ_CONFIGURATION_FILE_1)
    tmp_path.joinpath("file2.yml").write_text(READ_CONFIGURATION_FILE_2)

    assert sorted(rollin.read_configuration(tmp_path)) == ["adipiscing", "amet", "consectetur", "dolor", "elit", "ipsum", "lorem", "sit"]
    assert capsys.readouterr().out == ""


def test_read_configuration_ko_oserror(tmp_path, monkeypatch, capsys):
    tmp_path.joinpath("file2.yml").write_text(READ_CONFIGURATION_FILE_2)
    monkeypatch.setattr(pathlib.Path, "iterdir", lambda _: [tmp_path.joinpath("file1.yml"), tmp_path.joinpath("file2.yml")])

    assert rollin.read_configuration(tmp_path) == ["consectetur", "adipiscing", "elit"]
    assert capsys.readouterr().out == "'file1.yml' ignored: [Errno 2] No such file or directory: '{0}/file1.yml'\n".format(tmp_path)


def test_read_configuration_ko_valueerror(tmp_path, capsys):
    tmp_path.joinpath("file1.yml").write_bytes(bytes([0x99]))
    tmp_path.joinpath("file2.yml").write_text(READ_CONFIGURATION_FILE_2)

    assert rollin.read_configuration(tmp_path) == ["consectetur", "adipiscing", "elit"]
    assert capsys.readouterr().out == "'file1.yml' ignored: 'utf-8' codec can't decode byte 0x99 in position 0: invalid start byte\n"


def test_read_configuration_ko_not_a_list(tmp_path, capsys):
    tmp_path.joinpath("file1.yml").write_text("lorem ipsum dolor sit amet")
    tmp_path.joinpath("file2.yml").write_text(READ_CONFIGURATION_FILE_2)

    assert rollin.read_configuration(tmp_path) == ["consectetur", "adipiscing", "elit"]
    assert capsys.readouterr().out == "'file1.yml' ignored: not a proper yaml or json list\n"


def test_read_configuration_ok_empty(tmp_path, capsys):
    assert rollin.read_configuration(tmp_path) == []
    assert capsys.readouterr().out == ""


def test_read_configuration_ok_json(tmp_path, capsys):
    tmp_path.joinpath("file1.json").write_text('["lorem", "ipsum", "dolor", "sit", "amet"]')
    tmp_path.joinpath("file2.yml").write_text(READ_CONFIGURATION_FILE_2)

    assert sorted(rollin.read_configuration(tmp_path)) == ["adipiscing", "amet", "consectetur", "dolor", "elit", "ipsum", "lorem", "sit"]
    assert capsys.readouterr().out == ""
