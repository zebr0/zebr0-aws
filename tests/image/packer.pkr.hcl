packer {
  required_plugins {
    lxd = {
      version = ">= 1.0.0"
      source  = "github.com/hashicorp/lxd"
    }
  }
}

source "lxd" "rollin" {
  image              = "images:ubuntu/jammy/cloud"
  output_image       = "ubuntu/jammy/cloud"
  publish_properties = {
    description = "made with packer to speed up the tests of the rollin project"
  }
}

build {
  sources = ["source.lxd.rollin"]

  provisioner "shell" {
    inline = [
      "add-apt-repository -y ppa:zebr0/omega",
      "apt-get -y install pummel",
      "apt-get -y install stayinalive",
      "apt-get -y install restic",
      "apt-get -y install systemd-journal-remote",
      "apt-get -y install nginx",
      "apt-get -y install python3-flask",
      "apt-get -y install at"
    ]
  }
}
