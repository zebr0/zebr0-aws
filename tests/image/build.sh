#!/bin/bash -ex

lxc image delete ubuntu/jammy/cloud || true
sudo distrobuilder build-lxd distrobuilder.yaml
lxc image import lxd.tar.xz rootfs.squashfs --alias ubuntu/jammy/cloud
sudo rm lxd.tar.xz rootfs.squashfs
