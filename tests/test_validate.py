import rollin


def test_validate_clients_ok(capsys):
    clients = [
        {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "false"},
        {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "force"}
    ]

    assert rollin.validate(clients) == clients
    assert capsys.readouterr().out == ""


VALIDATE_TESTS_KO_OUTPUT = """
ignored, not a dictionary: "string"
ignored, not a dictionary: []
ignored, keys must match {"url", "failover", "levels", "extra_vars", "timeout", "cache", "pattern", "ca", "disabled"}: {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned"}
ignored, keys must match {"url", "failover", "levels", "extra_vars", "timeout", "cache", "pattern", "ca", "disabled"}: {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "false", "unexpected": "parameter"}
ignored, keys must match {"url", "failover", "levels", "extra_vars", "timeout", "cache", "pattern", "ca", "disabled"}: {}
ignored, value for "url" can only be a string: {"url": [], "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"}
ignored, value for "failover" can only be a string: {"url": "https://hub.zebr0.io", "failover": [], "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"}
ignored, value for "levels" can only be a list of strings: {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": "project", "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"}
ignored, value for "levels" can only be a list of strings: {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project", 1], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"}
ignored, value for "extra_vars" can only be a dictionary of strings: {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": "value", "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"}
ignored, value for "extra_vars" can only be a dictionary of strings: {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": 1}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"}
ignored, value for "timeout" can only be a float: {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "true", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"}
ignored, value for "cache" can only be an integer: {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": 2.1, "cache": "true", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"}
ignored, value for "pattern" can only be a string: {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": 2.1, "cache": 1, "pattern": 1, "ca": "selfsigned", "disabled": "true"}
ignored, value for "ca" can only be a string: {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": 2.1, "cache": 1, "pattern": "rollin_lxd", "ca": 1, "disabled": "true"}
ignored, value for "disabled" can only be "false", "true" or "force": {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": 2.1, "cache": 1, "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "yes"}
""".lstrip()


def test_validate_tests_ko(capsys):
    assert rollin.validate([
        "string",
        [],
        {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned"},
        {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "false", "unexpected": "parameter"},
        {},
        {"url": [], "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"},
        {"url": "https://hub.zebr0.io", "failover": [], "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"},
        {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": "project", "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"},
        {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project", 1], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"},
        {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": "value", "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"},
        {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": 1}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"},
        {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "true", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"},
        {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "true", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "true"},
        {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": 1, "ca": "selfsigned", "disabled": "true"},
        {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": 1, "disabled": "true"},
        {"url": "https://hub.zebr0.io", "failover": "https://hub.zebr0.io", "levels": ["project"], "extra_vars": {"key": "value"}, "timeout": "2.1", "cache": "1", "pattern": "rollin_lxd", "ca": "selfsigned", "disabled": "yes"}
    ]) == []
    assert capsys.readouterr().out == VALIDATE_TESTS_KO_OUTPUT


def test_validate_tests_ok_empty(capsys):
    assert rollin.validate([]) == []
    assert capsys.readouterr().out == ""
