import random
import string
import subprocess
import threading
import time

import kivalu
import pytest
import requests
from blinker import signal

import rollin


@pytest.fixture(scope="module")
def server():
    with kivalu.TestServer(address="0.0.0.0") as server:
        yield server


@pytest.fixture(autouse=True)
def clean_before_and_after():
    def clean():
        subprocess.run("lxc stop rollin", shell=True)
        subprocess.run("lxc delete rollin", shell=True)
        subprocess.run("lxc profile delete rollin", shell=True)
        subprocess.run("lxc network delete rollin", shell=True)
        subprocess.run("lxc storage volume delete rollin rollin", shell=True)
        subprocess.run("lxc storage delete rollin", shell=True)

    clean()
    yield  # see https://stackoverflow.com/questions/22627659/run-code-before-and-after-each-test-in-py-test
    clean()


def string_gen(size, chars=string.ascii_lowercase):
    return "".join(random.choice(chars) for _ in range(size))


SCRIPT = """
---
- content: |
    ---
    - test: grep nominal /var/stayinalive/mode
      fix: while pgrep pummel > /dev/null; do sleep 1; done && sleep 0.1
      mode: init
      order: 10
  target: /etc/stayinalive/init.yaml
- apt-get -y install stayinalive

- sed -i "1i export RESTIC_REPOSITORY={{ (pattern ~ "-restic-repository") | get("/var/tmp/backup") }}" /root/.bashrc
- sed -i "1i export RESTIC_PASSWORD=changeme" /root/.bashrc
- apt-get -y install restic
- restic init || true

- include: script-ssl-{{ ca | default("selfsigned") }}

- apt-get -y install systemd-journal-remote
- sed -i "s/ListenStream=19531/ListenStream=127.0.0.1:19531/g" /lib/systemd/system/systemd-journal-gatewayd.socket
- systemctl enable --now systemd-journal-gatewayd.socket

- content: |
    server {
      listen 2501 ssl;
      ssl_certificate /etc/ssl/private/fullchain.pem;
      ssl_certificate_key /etc/ssl/private/privkey.pem;
      root /var/stayinalive;
      try_files /status.json =404;
    }
    server {
      listen 2502 ssl;
      ssl_certificate /etc/ssl/private/fullchain.pem;
      ssl_certificate_key /etc/ssl/private/privkey.pem;
      location / {
        proxy_pass http://127.0.0.1:19531;
      }
    }
  target: /etc/nginx/sites-enabled/default
- apt-get -y install nginx

- include: script-app

- echo nominal > /var/stayinalive/mode
- systemctl reboot
""".lstrip()

SCRIPT_SSL_SELFSIGNED = """
---
- openssl req -x509 -newkey rsa:4096 -keyout /etc/ssl/private/privkey.pem -out /etc/ssl/private/fullchain.pem -sha256 -nodes -subj="/CN=localhost"
""".lstrip()

SCRIPT_APP = """
---
- useradd --system dork
- mkdir /var/dork
- chown dork:dork /var/dork

- restic restore latest --target / --path /var/dork || true

- apt-get -y install python3-flask
- content: |
    from pathlib import Path
    from time import time
    from flask import Flask, request, json, jsonify

    app = Flask(__name__)

    @app.route("/", methods=["POST"])
    def post():
        Path("/var/dork").joinpath(str(time()) + ".dork").write_text(json.dumps(request.get_json()), encoding="utf-8")
        return "", 201

    @app.route("/")
    def get():
        return jsonify([json.loads(i.read_text(encoding="utf-8")) for i in sorted(Path("/var/dork").iterdir()) if i.is_file() and i.name.endswith(".dork")])
  target: /opt/dork/dork.py
- content: |
    [Unit]
    Description=dork
    After=network-online.target

    [Service]
    Type=simple
    User=dork
    WorkingDirectory=/opt/dork
    Environment=FLASK_APP=dork.py
    ExecStart=flask run
    Restart=always
  target: /etc/systemd/system/dork.service
- content: |
    server {
      listen 443 ssl;
      ssl_certificate /etc/ssl/private/fullchain.pem;
      ssl_certificate_key /etc/ssl/private/privkey.pem;
      location / {
        proxy_pass http://127.0.0.1:5000;
      }
    }
  target: /etc/nginx/sites-enabled/dork

- content: |
    ---
    - test: systemctl is-active dork
      fix: systemctl start dork && sleep 1
      mode: nominal
      order: 10
    - test: "! systemctl is-active dork"
      fix: systemctl stop dork && sleep 1
      mode: rollin
      order: 10
    - test: test -f /var/tmp/dork-backup-done
      fix: restic backup /var/dork && touch /var/tmp/dork-backup-done
      mode: rollin
      order: 20
  target: /etc/stayinalive/dork.yaml

- apt-get -y install at
- echo "echo rollin > /var/stayinalive/mode" | at -t $(date --date "70 seconds" "+%m%d%H%M.%S")
""".lstrip()

CONFIGURATION_DEFAULT = """
---
- url: http://192.168.42.1:8000
  failover: http://localhost:8000
  levels: []
  extra_vars: {{}}
  timeout: 1
  cache: 1
  pattern: rollin_lxd
  ca: selfsigned
  disabled: {0}
""".lstrip()

OUTPUT_DEFAULT = """
creating /1.0/storage-pools/rollin
creating /1.0/storage-pools/rollin/volumes/custom/rollin
creating /1.0/networks/rollin
creating /1.0/profiles/rollin
creating /1.0/instances/rollin
starting /1.0/instances/rollin
stopping /1.0/instances/rollin
deleting /1.0/instances/rollin
creating /1.0/instances/rollin
starting /1.0/instances/rollin
stopping /1.0/instances/rollin
deleting /1.0/instances/rollin
creating /1.0/instances/rollin
starting /1.0/instances/rollin
stopping /1.0/instances/rollin
deleting /1.0/instances/rollin
deleting /1.0/profiles/rollin
deleting /1.0/networks/rollin
deleting /1.0/storage-pools/rollin/volumes/custom/rollin
deleting /1.0/storage-pools/rollin
exiting: loop-ending signal
""".lstrip()


def test_default(server, tmp_path, capsys):
    logs_directory = tmp_path.joinpath("logs")
    logs_directory.mkdir()

    server.data = {
        "script": SCRIPT,
        "script-ssl-selfsigned": SCRIPT_SSL_SELFSIGNED,
        "script-app": SCRIPT_APP,
        "rollin-logging-file-directory": str(logs_directory)
    }

    configuration_directory = tmp_path.joinpath("configuration")
    configuration_directory.mkdir()
    configuration_file = configuration_directory.joinpath("file")
    configuration_file.write_text(CONFIGURATION_DEFAULT.format("false"))

    i = 0
    data = []

    @signal("started").connect
    def on_start(client: rollin.Client):
        nonlocal i
        i += 1
        if i > 2:
            def sleep_then_disable():
                time.sleep(1)
                configuration_file.write_text(CONFIGURATION_DEFAULT.format("true"))

            threading.Thread(target=sleep_then_disable).start()

        while True:
            item = {"i": i, "value": string_gen(32)}
            if not requests.post(f"https://{client.get_address()}", json=item, verify=False).ok:
                break
            data.append(item)
            response = requests.get(f"https://{client.get_address()}", verify=False)
            if not response.ok:
                break
            assert response.json() == data

    event = threading.Event()

    @signal("deleted").connect
    def stop(_):
        event.set()

    rollin.rollin(configuration_directory, event, 1)

    assert capsys.readouterr().out == OUTPUT_DEFAULT

    stats = {}
    for d in data:
        stats[d["i"]] = stats.get(d["i"], 0) + 1

    assert set(stats.keys()) == {1, 2, 3}
    for value in list(stats.values()):
        assert value > 10

    text_to_find = [
        "Journal started",
        "Starting Initial cloud-init job",
        "fetching script 'script' from server",
        "System is rebooting",
        "Journal started",
        "active mode has changed: nominal",
        "Started dork",
        "active mode has changed: rollin",
        "Stopped dork",
        "[20] fixing: restic backup",
        "[20] fix successful"
    ]
    for log_path in sorted(logs_directory.iterdir()):
        i = 0
        with open(log_path) as log_file:
            for line in log_file:
                if text_to_find[i] in line:
                    i += 1
                    if i == len(text_to_find):  # if we're at the end of the array, we don't need to read the rest of the file
                        break

        assert i == len(text_to_find), f'"{text_to_find[i]}" not found in file {log_path}'
