import rollin


def test_set_defaults_ok():
    assert rollin.set_defaults([{}]) == [{'ca': 'selfsigned',
                                          'pattern': 'rollin_lxd',
                                          'extra_vars': {'ca': 'selfsigned', 'pattern': 'rollin_lxd'}}]
    assert rollin.set_defaults([{'ca': 'letsencrypt'}]) == [{'ca': 'letsencrypt',
                                                             'pattern': 'rollin_lxd',
                                                             'extra_vars': {'ca': 'letsencrypt', 'pattern': 'rollin_lxd'}}]
    assert rollin.set_defaults([{'pattern': 'rollin_openstack'}]) == [{'ca': 'selfsigned',
                                                                       'pattern': 'rollin_openstack',
                                                                       'extra_vars': {'ca': 'selfsigned', 'pattern': 'rollin_openstack'}}]
    assert rollin.set_defaults([{'extra_vars': {'key': 'value'}}]) == [{'ca': 'selfsigned',
                                                                        'pattern': 'rollin_lxd',
                                                                        'extra_vars': {'key': 'value', 'ca': 'selfsigned', 'pattern': 'rollin_lxd'}}]
    assert rollin.set_defaults([{'extra_vars': {'pattern': 'wrong'}}]) == [{'ca': 'selfsigned',
                                                                            'pattern': 'rollin_lxd',
                                                                            'extra_vars': {'ca': 'selfsigned', 'pattern': 'rollin_lxd'}}]
