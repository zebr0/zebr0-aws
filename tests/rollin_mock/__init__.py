from __future__ import annotations

import http.server
import threading
import time

import kivalu
import requests

import rollin

ADDRESS = "127.0.0.1"
PORT = 8000

CREATE = "create"
ROLLIN = "rollin"
DELETE = "delete"

EXISTS = "exists"
ROLLIN_COUNT = "rollin_count"


class TestServer:
    def __init__(self, address: str = ADDRESS, port: int = PORT) -> None:
        self.data = {}

        class RequestHandler(http.server.BaseHTTPRequestHandler):
            def do_GET(zelf):
                stack, action = zelf.path[1:].split("/")  # minus leading "/"

                if action == CREATE:
                    if self.data.get(stack, {}).get(EXISTS, False):
                        print(stack, "already exists")
                    else:
                        print(stack, "creating")
                        time.sleep(0.9)
                        self.data.setdefault(stack, {}).update({EXISTS: True})
                        print(stack, "created")
                elif action == ROLLIN:
                    if self.data.get(stack, {}).get(ROLLIN_COUNT, 0) < 3:
                        self.data.setdefault(stack, {}).update({ROLLIN_COUNT: self.data.get(stack, {}).get(ROLLIN_COUNT, 0) + 1})
                        print(stack, "not rollin yet")
                    else:
                        print(stack, "rollin")
                        time.sleep(0.9)
                        self.data.setdefault(stack, {}).update({ROLLIN_COUNT: 0})
                        print(stack, "rolled")
                elif action == DELETE:
                    if self.data.get(stack, {}).get(EXISTS, False):
                        print(stack, "deleting")
                        time.sleep(0.9)
                        self.data.setdefault(stack, {}).update({EXISTS: False})
                        print(stack, "deleted")
                    else:
                        print(stack, "already deleted")

                zelf.send_response(200)
                zelf.end_headers()

        self.server = http.server.ThreadingHTTPServer((address, port), RequestHandler)

    def start(self) -> None:
        threading.Thread(target=self.server.serve_forever).start()

    def stop(self) -> None:
        self.server.shutdown()
        self.server.server_close()

    def __enter__(self) -> TestServer:
        self.start()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.stop()


class Client(rollin.Client):
    def __init__(self, kivalu_client: kivalu.Client):
        super().__init__(kivalu_client)

        self.http_session = requests.Session()
        self.base_url = f'http://{ADDRESS}:{PORT}/{"-".join(self.kivalu_client.levels)}/'

    def create(self):
        self.http_session.get(self.base_url + CREATE)

    def rollin_if_ready(self):
        self.http_session.get(self.base_url + ROLLIN)

    def delete(self):
        self.http_session.get(self.base_url + DELETE)
