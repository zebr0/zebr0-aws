import importlib
import json
import logging.config
import threading
import time
from pathlib import Path
from typing import List, Any

import kivalu
import requests
import yaml
from blinker import signal

DELAY_DEFAULT = 10


def read_configuration(directory: Path) -> List[Any]:
    """
    Extracts every JSON or YAML configuration item from the files in the given directory.

    :param directory: Path to the configuration directory
    :return: aggregated list of configuration items
    """

    items = []

    for path in directory.iterdir():
        try:
            text = path.read_text(encoding="utf-8")
        except (OSError, ValueError) as error:
            print(f"'{path.relative_to(directory)}' ignored: {error}")
            continue

        data = yaml.load(text, Loader=yaml.BaseLoader)
        if not isinstance(data, list):
            print(f"'{path.relative_to(directory)}' ignored: not a proper yaml or json list")
        else:
            items.extend(data)

    return items


def validate(items: List[Any]) -> List[dict]:
    """
    Validates the given configuration items.

    :param items: list of configuration items
    :return: a list of valid configuration items
    """

    result = []

    for item in items:
        if not isinstance(item, dict):
            print("ignored, not a dictionary:", json.dumps(item))
            continue

        if item.keys() != {"url", "failover", "levels", "extra_vars", "timeout", "cache", "pattern", "ca", "disabled"}:
            print('ignored, keys must match {"url", "failover", "levels", "extra_vars", "timeout", "cache", "pattern", "ca", "disabled"}:', json.dumps(item))
            continue

        if not isinstance(item["url"], str):
            print('ignored, value for "url" can only be a string:', json.dumps(item))
            continue

        if not isinstance(item["failover"], str):
            print('ignored, value for "failover" can only be a string:', json.dumps(item))
            continue

        if not isinstance(item["levels"], list) or any(not isinstance(value, str) for value in item["levels"]):
            print('ignored, value for "levels" can only be a list of strings:', json.dumps(item))
            continue

        if not isinstance(item["extra_vars"], dict) or any(not isinstance(key, str) or not isinstance(value, str) for key, value in item["extra_vars"].items()):
            print('ignored, value for "extra_vars" can only be a dictionary of strings:', json.dumps(item))
            continue

        try:
            item["timeout"] = float(item["timeout"])
        except (ValueError, TypeError):
            print('ignored, value for "timeout" can only be a float:', json.dumps(item))
            continue

        try:
            item["cache"] = int(item["cache"])
        except (ValueError, TypeError):
            print('ignored, value for "cache" can only be an integer:', json.dumps(item))
            continue

        if not isinstance(item["pattern"], str):
            print('ignored, value for "pattern" can only be a string:', json.dumps(item))
            continue

        if not isinstance(item["ca"], str):
            print('ignored, value for "ca" can only be a string:', json.dumps(item))
            continue

        if not item["disabled"] in ["false", "true", "force"]:
            print('ignored, value for "disabled" can only be "false", "true" or "force":', json.dumps(item))
            continue

        result.append(item)

    return result


def set_defaults(items: List[dict]) -> List[dict]:
    for item in items:
        pattern, ca = item.setdefault("pattern", "rollin_lxd"), item.setdefault("ca", "selfsigned")
        item.setdefault("extra_vars", {}).update({"pattern": pattern, "ca": ca})

    return items


class Client:
    def __init__(self, kivalu_client: kivalu.Client):
        self.kivalu_client = kivalu_client

    def create(self):
        raise NotImplementedError()

    def get_address(self):
        raise NotImplementedError()

    def rollin_if_ready(self):
        raise NotImplementedError()

    def delete(self):
        raise NotImplementedError()


LOGGING_CONFIG_DEFAULT = """
{% set name = "-".join(levels) or "rollin" %}
---
handlers:
  {{ name }}:
    class: logging.FileHandler
    filename: {{ "rollin-logging-file-directory" | get("/var/tmp") }}/{{ name }}_{{ "rollin-logging-file-datepattern" | get("%Y%m%d-%H%M%S") | now }}.log
loggers:
  {{ name }}:
    handlers: ["{{ name }}"]
    level: INFO
""".lstrip()


@signal("started").connect
def log_sink(client: Client):
    config = yaml.load(client.kivalu_client.get("rollin-logging-config", LOGGING_CONFIG_DEFAULT), Loader=yaml.BaseLoader)
    config.update({"version": 1, "disable_existing_loggers": False})
    logging.config.dictConfig(config)
    loggers = [logging.getLogger(key) for key in config.get("loggers").keys()]

    def _():
        response = requests.get(
            f"https://{client.get_address()}:2502/entries?follow",
            stream=True,
            verify=client.kivalu_client.extra_vars.get("ca") != "selfsigned",
            headers={"Accept": client.kivalu_client.get("rollin-logging-format", "text/plain")})
        for line in response.iter_lines(decode_unicode=True):
            for logger in loggers:
                logger.info(line)

    threading.Thread(target=_).start()


class RollinThread(threading.Thread):
    def __init__(self, configuration) -> None:
        super().__init__()
        self.configuration = configuration

    def run(self) -> None:
        try:
            client: Client = importlib.import_module(self.configuration.get("pattern")).Client(kivalu.Client(**self.configuration))

            if self.configuration.get("disabled") != "false":
                client.delete()
            else:
                client.create()
                client.rollin_if_ready()
        except Exception as exc:
            print(repr(exc))


def is_already_running(configuration):
    for thread in threading.enumerate():
        if isinstance(thread, RollinThread) and thread.configuration == configuration and thread.is_alive():
            return True
    return False


def rollin(configuration_directory: Path, event: threading.Event, delay: float = DELAY_DEFAULT) -> None:
    if not configuration_directory.is_dir():
        print("exiting:", configuration_directory, "must exist and be a directory")
        return

    while not event.is_set():
        for item in set_defaults(validate(read_configuration(configuration_directory))):
            if not is_already_running(item):
                RollinThread(item).start()
                time.sleep(0.01)

        event.wait(delay)  # interruptable pause before the next iteration

    print("exiting: loop-ending signal")
